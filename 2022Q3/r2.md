\clearpage

# Report of the U.S. CMS Resource Manager

The funding provided by DOE and NSF to the U.S.\ CMS Operations Program for 2002
through 2022, as well as the funding projection for 2023 and 2024, is shown in
Figure {@fig:funding_profile}.  For DOE the projection shows the current guidance, while
for NSF it reflects the first part of the current Coooperative Agreement.

![The annual U.S.\ CMS Operations Program funding provided by DOE and NSF.  For 2002
through 2022 the chart shows the actual funding, while for 2023 and 2024 the current
guidance (DOE) and level of the on-going Cooperative Agreement (NSF) are shown.](figures/CY22_Funding_Profile.pdf){#fig:funding_profile}

Resources are distributed and tracked across the three areas through which the
Operations Program is implemented:  Detector Operations (DetOps), Software and
Computing (S&C), and Common Operations (ComOps). ComOps is a category for items that
would otherwise belong in both, or neither, of the other two categories.

Internal budget reviews for calendar year 2022 took place in August and September of 2021,
from which U.S.\ CMS Management developed a detailed spending plan,
while taking into account updated information from the funding agencies
obtained via Joint Oversight Group meetings and regular communications.

Primarily during the first quarter of the calendar year, Statement of Work (SOW)
agreements were established with each institution that is providing a deliverable in
exchange for Operations Program funding.  The SOWs specify the tasks to be carried out,
as well as any portions of salaries, materials and services (M&S), travel funding, or
cost of living adjustments (COLA) to be paid from the Operations Program budget.  The
SOWs must be approved by U.S.\ CMS Operations Program management, by the Fermilab
Director Designee, and by representatives of the collaborating group and institution.
Through September of 2022, a total of 84 SOWs (50 DOE and 34 NSF) were produced and
approved.  After a SOW is approved, any additional changes are considered and,
if approved, enacted through a Change Request procedure.

Figure {@fig:change_log} shows the Spending Plan Change Log which captures revisions
that were made prior to SOW approvals, as well as modifications implemented through
Change Requests.  The information is reported here down to the level-2 subsystem
categories within DetOps, S&C, and ComOps.  The CY22 spending plan, as of the end of Q3,
is shown for DOE and NSF funds in Figure {@fig:spending_plan}.

![Spending Plan Change Log for CY22 Q3.](figures/CY22Q3_Change_Log.pdf){#fig:change_log}

![Spending plan at the end of CY22 Q3, for funds from DOE, NSF, and the total.](figures/CY22Q3_Spending_Plan.pdf){#fig:spending_plan}

Once funds have been committed through purchase orders, in the
case of DOE, and sub-awards, in the case of NSF, they are considered obligated.
Figure {@fig:DOE_obligations} shows the obligations in the areas of DetOps, S&C, and
ComOps, as compared to the spending plan, for DOE funds.  The spending plan is plotted
as if expenditures are carried out in even allocations each month, but this is
intentionally not the case due to equipment purchases and the larger of the transfers
to CERN-based Team Accounts, the latter of which are targeted for when exchange rates
are favorable.  Spending at Fermilab has historically been budgeted according to the
fiscal year, however spending through Universities and CERN Team Accounts is budgeted and
tracked according to the calendar year, so the U.S.\ CMS Operations Program has
been reporting all activities based on calendar year.  Figure {@fig:NSF_obligations} shows
the total obligations and the spending plan, for NSF funds.  Of the $10.25M in NSF
funding, about $662.8k in subawards and amendments were obligated this quarter, in addition to approximately
$259.4k of spending more directly through Nebraska.

![Obligations and spending plan for DOE funds.  The spending plan is indicated with
the assumption of equal monthly increments just as a rough guide.](figures/CY22Q3_DOE_Obligations.pdf){#fig:DOE_obligations}

![Obligations and spending plan for NSF funds.  The spending plan is indicated with
the assumption of equal monthly increments as a rough guide.](figures/CY22Q3_NSF_Obligations.pdf){#fig:NSF_obligations}

Resources deployed at CERN, and paid directly in Swiss francs, account for approximately
23% of the 2022 spending plan.  This carries considerable exposure to the exchange rate.
Again this year, the spending plan was developed using an assumed
exchange rate of 0.977 CHF/USD (which is the same rate assumed by the US HL-LHC Project).
Furthermore, the difference between assuming 0.977 CHF/USD and 0.9 CHF/USD (which was the
old assumption) is set aside as Risk Contingency.  As items costed in Swiss francs are paid,
the budget is shifted among Risk Contingency, Management Reserve, and the spending plan
as appropriate to reflect the prevailing exchange rate.  The actual average exchange rate
in CY22 Q3 was 0.966 CHF/USD.  Figure {@fig:Team_Accounts} shows the allocated budgets and
year-to-date spending through the Team Accounts that are used for expenditures at CERN.
Spending for labor and cost of living adjustments occurs at a fairly constant rate.
Figure {@fig:Team_Accounts} does not include the M&O-A payments, as these
are made through multiple payments to a separate Team Account.
<!---
 Source for exchange rate average:
 http://www.oanda.com/currency/historical-rates/
 Go to historical, Enter USD and CHF, select dates, and look at *Table* to get the average
 Alternatively, use this:  https://www.investing.com/currencies/usd-chf-historical-data,
 or free to access:  https://www.ofx.com/en-us/forex-news/historical-exchange-rates/
-->


![Budget plan and year-to-date spending, in Swiss francs, through DetOps (top), ComOps (middle), and S&C (bottom) Team Accounts.](figures/CY22Q3_TA_All.pdf){#fig:Team_Accounts}


A Risk Management Plan is being implemented for the U.S.\ CMS Operations Program,
with many aspects drawn from the Fermilab Risk Management Plan.  A Risk Register is updated
quarterly, according to the workflow described in the following subsection.  At the start of
the quarter, the Risk Register contained 67 open risks spread across the program.  At the end of
quarter, there were 74 risks, with 60 threats, 13 uncertainties, and 1 opportunity.
Figure {@fig:Risk_Summary} shows the top few risks at the end of the
quarter, ranked by *Probability* $\times$ *Cost Impact*.

![Summary of the U.S.\ CMS Operations Program Risk Register.  Only risks with *Probability* $\times$ *Mean Cost Impact* $\ge$ $100k are shown.](figures/CY22Q3_Risk_Summary.pdf){#fig:Risk_Summary}

## Workflow for Risk Management Plan

The following procedures have been put in place to carry out the workflow for the U.S.\ CMS Operations Program Risk
Management Plan.  The workflow is divide into two paths:  (1) updates that are made at any time, and (2) a review
of risks once per quarter.  In all of the following, *updates* mean adding new risks, realizing risks, retiring old risks,
or modifying existing risks.  In all cases, it is the program office team that edits the Risk Register.  The following descriptions
are also summarized in Figure {@fig:Risk_Workflow}.

### (1) At any time:

Any member of the management team (including Program Manager, Deputy Program Manager, L1 Managers, L2 Managers,
Resource Manager, and program office lead) shall alert the program office of any updates.  The program office informs
the corresponding L1 manager, and the L1 manager approves, rejects, or modifies the proposed updates.  This can also
involve iterating with the L2 manager.  If updates are accepted, the management team reviews the risk mitigations and
risk responses that are associated with the updates.  The management team takes into account the risk rank and/or position
in the risk rank matrix and takes any necessary preemptive actions to incorporate mitigation activities into the plan.
If appropriate (again factoring in the probability and impact of the risk), the operations program plan is also adjusted
to account for the estimated resources required to execute the risk responses related to the updated risks.

### (2) Once per quarter:

Within one month prior to the end of each quarter, the program office lead asks the L2 managers whether they have any
updates to the risks in their L2 area.  The program office then informs the corresponding L1 manager of any such updates,
and the L1 manager approves, rejects, or modifies the proposed updates.  As above, this can involve iterating with the L2
manager.  As part of the quarterly workflow, the management team reviews *all* of the current risks, and takes any necessary
actions and adjusts the program plan if appropriate.

![Summary of the two Risk Management Plan workflow paths.](figures/USCMS_Risk_Workflow_figure.pdf){#fig:Risk_Workflow}

\clearpage
