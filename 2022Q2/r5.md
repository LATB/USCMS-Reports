\clearpage

# Detector Operations

In the past quarter, efforts were directed at preparing the detector for running with physics beams at a record energy of 13.6 TeV. By the end of the quarter all U.S. contributions to the experiment were ready for the collisions that were subsequently delivered on July 5.

## BRIL

The newly built pixel luminosity telescope (PLT) together with the new BCM1F and the BCML detectors and the luminosity measured with the HF detector successfully operated during the first Run 3 stable beam fills at 900 GeV, which were a great help to prepare/calibrate/optimize for 13.6 TeV collisions, and the first 13.6 TeV collisions. The PLT detector's 16 telescopes are all operational and completed the lengthy pixel trimming procedure, and continue to undergo tuning such as timing adjustment with LHC beams. This also includes continued threshold and HV scans to maintain the best working point. Furthermore, the PLT is prepared to publish luminosity for each of the channels independently. The online publication via BRILdaq for all instruments is established. The PLT now uses a 16 channel average, as during Run 2. All luminometers, PLT, BCM1F, and HF, agree within 1% of the published instantaneous luminosity after preliminary calibration of visible cross sections using LHC beam emittance scans. All the operation procedures for the PLT are documented, and dedicated training sessions for the Detector On-Call expert (DOC) shifters were held. To staff these shifts, the availability of BRIL personnel at CERN is critical.

The schedule for the assembly of the second replacement PLT, PLT-3, has been delayed due to shortage of personnel, limited access to workshop for repair of electronic parts, and the LHC test run that required full attention, with the underlying cause being the Covid-19 pandemic. All four PLT cassettes are now assembled and undergo cold testing in a dedicated freezer. A set of 5 opto-motherboards successfully completed the first round of testing which provides us with a set of the 4 required plus one spare. Two additional boards might not be recoverable. The milestones for the PLT-3 assembly in the table will not be met as originally presented, and likely delayed somewhat further due to the personnel focus on the LHC startup. Nevertheless, extended cold-testing will improve the reliance of the components that are only needed for the assembly during the next shutdown.

A publication describing the PLT system and its operations during Run 2 has
been submitted to Eur.Phys.J. C and is posted on [arXiv 2206.08870](https://arxiv.org/abs/2206.08870).

  -----------------------------------------------------------
  Subsystem   Description               Scheduled    Achieved
  ----------- ------------------------ ----------- ----------
  BRIL        Fully calibrated PLT 2    Feb 15      Feb 15

  BRIL        PLT 3 quadrants           Feb 15      Mar 20
              assembled

  BRIL        PLT 2 ready for data      Mar 15      Mar 15
              taking

  BRIL        PLT 3 quadrants stress    Apr 31      ongoing
              tested

  BRIL        PLT 2 first luminosity    Jun 15      Jun 15
              calib

  BRIL        PLT 2 full lumi calib     Oct 31

  BRIL        PLT 2 luminosity          Nov 30
              analysis

  BRIL        1st year offline          Dec 31
              analysis
  -----------------------------------------------------------

  : BRIL 2022 Milestones


## Tracker

The tracker participated in cosmic running and beam collisions while remaining off during LHC beam scrubbing and during short periods between scrubbing runs. The latter is done to reduce the thermal shock to the Silicon Strip Detector and to halve the coolant leak rate by increasing the detector temperature to $-15^\circ$ C. The temperature of the detector was lowered to $-20^\circ$ C in early April. The Strip Detector developed increased coolant leakage and the search for leaks was re-initiated. In May the detector temperature was increased when it was discovered that ice was forming in one of the cooling plant cabinets. After thawing out, the leak search completed, and cooled components of the detector were re-commissioned prior to stable beams at the end of May. Several cooling loops were identified as especially leaky and closed, leading to a reduction in the leak rate from 16-20 kg/day to about 10 kg/day. In general, the tracker is kept as cool as possible to increase the working life of the silicon sensors.

In 2022, after LS2, the tracker volume dew-point is more closely tied to the experimental cavern dew-point than it had been in 2018. This became an issue during an especially high humidity period in June when we raised the detector temperatures to avoid ice build up in the tracker volume. There were some interventions in late June undertaken to recover from a membrane servicing and to improve the humidity situation in the detector volume and in the cooling cabinets by adjusting dry air flows. Further, the cavern dew-point has now been lowered by about $5^\circ$ C to help the situation. We will need to continue to monitor the tracker dew-point and determine if anything can be done during year end technical stops to help the humidity situation.

Both Strip and Pixel Detectors performed timing and bias scans with collisions to prepare for scans at nominal energy in July. Calibrations with beam off were completed for the Pixel Detector in early June, but the calibration of the modules in the Strip Detector where there is no active cooling was waiting for a few hours where we can turn on the high voltage without beam in the machine with the strips temperature set at $-20^\circ$ C. We expect both tracking detectors to be ready for physics after the full timing and bias scans are digested in mid-July, well ahead of the completion of the intensity ramp-up expected for mid-August.

  -----------------------------------------------------------
  Subsystem   Description                Scheduled   Achieved
  ----------- ------------------------ ----------- ----------
  Tracker     Pixel and Strips             Jun 15    see text
              calibrations without
              beam complete

  Tracker     Pixel and Strips ready       Apr 1      Apr 6
              for beam

  Tracker     Pixel and Strips ready       Jun 30
              for physics
  -----------------------------------------------------------

  : Tracker 2022 Milestones

                                         Pixels            Strips
  ------------------------------------- -------- ---------------------------
  Working channels in %                  > 97       $\approx$ 96
  Downtime attributed in pb$^{-1}$        n/a                n/a
  Fraction of downtime attributed (%)     n/a                n/a

  : Tracker Metrics


## ECAL

The ECAL fully participated in the global runs earlier in the quarter and in the 900 GeV running later in the quarter. The detector worked well with only minor issues that were easily fixed. In the 900 GeV two bunch colliding run ECAL accumulated 5.9 nb$^{-1}$ in 54 hours of running. The RecHit distribution looked as expected and plots of the $\pi^{0}$ invariant mass showed the preliminary calibration was quite close to correct. This early data is being used to improve the calibration.

The trigger spike suppression algorithm has been re-optimized for Run 3 and also more frequent laser calibrations are planned to better track the degradation in light yield from radiation damage. Spikes are anomalous deposits that result from the passage of hadronic particles through the avalanche photo-detectors. Spikes account for 20% of the trigger bandwidth in normal running. The radiation damage is rate dependent and anneals at room temperature so the light yield is constantly changing. A new feature to use "double weight" in the trigger primitive has been deployed. The trigger primitive which is a 5x5 crystal object is formed on detector in the ECAL front-end board using the FENIX ASIC. It was realized that the ASIC allows a second parallel amplitude filter to be implemented in addition to the standard one but with different weights. The second filter can be optimized to tag late out-of-time spikes which have been a problem in Run 2. Trigger emulations show improved spike rejection using this new technique. 900 GeV collisions were used to validate the procedure and compare against the trigger emulation. This resulted in finding and removing a bug in the trigger emulator. Early results are promising but tests are continuing.

The ECAL barrel has 62,000 crystals in the Barrel and 12,000 crystals in the endcap. There are 60 known noisy or problematic crystals in the barrel and eight in the endcap. These have been confirmed and masked in the present run. In addition there are three unrecoverable bad trigger towers (5x5 crystals) in the barrel and two in the endcap and we have confirmed again that these are unrecoverable.

A significant amount of work has been done in LS2 to automate the prompt calibration procedure. The goal is to provide a prompt calibration comparable to the optimized calibration used for the final Run 2 results. The ECAL calibration procedure consists of multiple steps and is a rather involved process, so this is an ambitious project. The workflow is implemented as a finite state machine that also includes a monitoring process. It was deployed for the first time for the 900 GeV collider run. All elements of the workflow were included except the electron calibration, which requires W/Z production not available at 900 GeV. The full calibration took 10 hours, which is acceptable, and the system will be refined as more data is accumulated.

The ECAL laser barracks is located underground at SX5. The space where it is presently located is required for use by the C02 cooling plant for the HGCal and MTD upgrades for Run 4. Work needs to begin on this in 2023. The laser barrack is therefore being moved to the surface, which requires some civil engineering for the new location and the installation of 100m of optical fibers, as well as a new more powerful laser to overcome the attenuation of the extra fibers. The civil engineering work is proceeding with some delay due to procurement delays. A CERN tender for the fibres yielded a bid for 100 KChF which was significantly below the expected cost of 250 KChF. However when the order was placed the company returned to say the original bid was a mistake and they wanted 1 MChF instead. A second tender yielded a quote of 237 KChF and the order has been successfully placed. Delivery is expected in mid November. The new laser has been received from Photonics in the USA and is being tested by Caltech engineers. It has failed to meet the jitter spec of 3ps and has been returned to the company for remediation. We expect the system can be installed in the Year End Technical Stop in December 2023.

  ------------------------------------------------------------------
  Subsystem   Description                  Scheduled      Achieved
  ----------- ------------------------ --------------- -------------
  ECAL        Complete Cosmic Ray Run    Mar 2022       Mar 2022
              with Magnet

  ECAL        Complete low intensity     Apr 2022       May 2022
              beam run

  ECAL        New ECAL laser barracks     Feb 2022      Postponed to
              ready to move into                            Jan 2023

  ECAL        laser/fiber installation      Mar 2022    Postponed to
              in barracks complete                          Jan 2023

  ECAL        laser/fiber commisioning     Mar 2022     Postponed to
              in barracks complete                          Jan 2023

  ECAL        ECAL ready for CMS           Mar 2022     Mar-2022
              Cavern Close

  ECAL        ECAL ready for beam          Apr 2022     Apr-2022

  ECAL        ECAL ready 1200 bunch        Jul 2022
              running
  ------------------------------------------------------------------

  : ECAL 2022 Milestones


## HCAL

HCAL preparations for physics collisions continued to progress well this past quarter, with generally good and consistent performance during stable beams. Analysis of preliminary collision data show good response across all the sub-detectors (HB/HE/HF/HO). The proper timing of the detectors has posed some challenges, particularly with HF, which observed significant timing shifts when crates were power cycled. This issue has been resolved with a rebuilt version of the ngCCM firmware, although investigations continue as we try to pin down the exact cause. Certain channels of HE required modifications to the timing alignment beyond the limits available in the front end, so more extensive adjustments had to be performed. The fine-tuning of the HB timing continues as well. Furthermore, non-physical collisions are appearing in HB in collision data, which disappear when the calibration sequence (pedestal + LED + laser) is disengaged. For now, the calibration sequence has been disabled until a fix in the firmware can be implemented.

The new HO slow control has been commissioned and now configures quickly and reliably. A semi-clean room was installed in B904 with costs shared by technical coordination and U.S. CMS operations, which will greatly help in the continuing activities there. The L1 trigger primitive calculations with out-of-time PU mitigation are still being fine tuned. They are currently causing L1 jet trigger pre-firing, probably due to a problem in the firmware, and will be rolled back until the issue is resolved.

  ---------------------------------------------------------------------------
  Subsystem   Description                             Scheduled      Achieved
  ----------- ----------------------------------- ------------- -------------
  HCAL        HB/HE/HO ready for global             Feb 2022      Feb-2022
              commissioning runs

  HCAL        HF detector moved out of garages      Mar 2022      Mar-2022
              and into position for Run 3

  HCAL        HF low voltage power supplies         Mar 2022      Mar-2022
              migrated from current HF racks to
              new location in X0

  HCAL        New slow-control system for HO        Mar 2022      Feb-2022
              operational

  HCAL        All HCAL detectors ready for CMS      Mar 2022      Mar-2022
              cavern close

  HCAL        VME Slow-control crate for HO         Mar 2022      Apr-2022
              detector commissioned

  HCAL        All HCAL detectors ready for LHC      Apr 2022      Apr-2022
              beam commissioning

  HCAL        HCAL detector timed-in with initial   Jul 2022      see text
              channel calibration

  HCAL        All HCAL detectors ready for          Jul 2022
              collisions with 1200 bunches

  HCAL        ZDC prepared for heavy-ion run        Nov 2022
  ---------------------------------------------------------------------------

  : 2022 HCAL Operations Milestones

## EMU

This quarter began with the continuation of the Cosmic Run at Four Tesla (CRAFT) and finished with stable collision runs at 900 GeV. The full CSC and GE1/1 systems participated in global runs in both periods. The 900 GeV collision runs were particularly useful for confirming and adjusting the timing settings of the CSC system. These runs were used to test firmware updates and to refine timing parameters in final preparation for 13.6 TeV collisions in July.

An extensive firmware update campaign was completed this quarter. The firmware updates for Run 3 were completed for the OTMBs, TMBs, and ALCTs. These updates implement upgrades targeted for Run 3: more precise trigger primitives, GEM-CSC combined trigger primitives, and a high multiplicity trigger (HMT). The HMT is a new feature to increase sensitivity to long-lived particles which may shower in the muon system. The firmware was tested extensively at B904 before deployment at P5, where it was then tested in local runs and global runs. All firmware was deployed at P5 by May 10, at which point the CSC system was declared ready for collisions.

The adjustment of online timing parameters for the CSC was also a major effort this quarter. The first beam splash events in April were used for a crude check of the timing, and then the settings were refined with the 900 GeV collisions in May and June. The early 900 GeV runs suffered from large rates of beam halo on the minus side. The halo was largely eliminated with LHC collimator adjustments in mid-June. After applying corrections from these data, the CSC system is ready to start 13.6 TeV collisions with all chambers timed in to better that 5 ns. The settings can be adjusted further with the early runs in July, along with the offline timing settings.

In May, a critical CERN-wide shortage of CF$_4$ gas became apparent. The CSC chambers usually operate with a gas mixture containing 10% CF$_4$, and the supplies available at CERN were projected to run out in September with no apparent prospects for delivery of new gas. In response, the CSC system switched to a mixture of 5% CF$_4$, where 2.5% comes from recovered gas, and 2.5% comes from a fresh supply. In addition, a plan was agreed on to shut off the CSC system entirely for three weeks in June, but this interruption was cancelled when additional bottles of gas were secured from Italy and elsewhere. This crisis is temporarily averted by the additional supply, but it is an indication of the sort of CF$_4$ supply problems that may be encountered in the future.

Finding a path to reduce or eliminate CF$_4$ is one of the main purposes of the CSC chamber aging tests at at the GIF++ radiation facility. During this quarter, an ME2/1 chamber was prepared for aging tests with a gas mixture with 5% CF$_4$ in a closed gas loop. Note that an ME1/1 chamber with 5% CF$_4$ is also under test at GIF++, and no degradation has been seen. This reduced level of CF$_4$ was targeted for Run 4, but it is also being considered for Run 3 if the tests reveal no problems. In addition, a new set of CSC mini chambers was assembled for use in exploring more alternative gasses.

  ------------------------------------------------------------------------
  Subsystem   Description                         Scheduled   Achieved
  ----------- ----------------------------------- ----------- ------------
  EMU         Technical proposal for adapting to    Feb 2022    Feb 2022
              CMS Phase 2 TCDS released to CSC
              community

  EMU         DPG: Comparison of FLUKA simulated    Mar 2022    Mar 2022
              background to Run 2 data reported
              to EMU community

  EMU         GEM/CSC trigger exercised in global   Mar 2022    Apr 2022
              run in one end cap

  EMU         Final version OTMB firmware           Mar 2022    see text
              deployed in ME1234/1

  EMU         CSC system ready for collisions       Jun 2022    May 2022

  EMU         HV trip recover mechanism             Aug 2022
              re-optimised

  EMU         Trigger timing fine adjustment        Oct 2022
              using stable beam data

  EMU         LCT and segment efficiencies          Nov 2022
              measured in early Run 3 data

  EMU         Spatial and timing resolutions        Nov 2022
              measured in early Run 3 data

  EMU         Gas gain uniformity checked           Dec 2022

  EMU         HV current stability analyzed         Dec 2022

  EMU         SEU monitoring analysed               Dec 2022

  EMU         ODMB7/FED demonstrator setup          Dec 2022

  EMU         GEM-CSC trigger efficiency measured   Dec 2022
              in Run 3 data

  EMU         5% CF4 full size chamber              Dec 2022
              irradiation finished
  ------------------------------------------------------------------------

  : EMU 2022 Milestones


## DAQ

The transition of the central DAQ system to DAQ3 has been completed. The group has been working on the commissioning of DAQ3 with beams. The storage and transfer system is full commissioned. First beam splashes took place on April 22. DAQ was operated in the splash configuration. The initial operation was successful but some network issues were revealed and later resolved. The first stable beam took place on Jun 2nd at 900 GeV, where DAQ back pressure was observed at high rate ($>90$ kHz) during the commissioning. The DAQ backpressure was not observed any longer after increasing the D2S receive buffers and the FIFO capacity. A new configuration incorporating the changes was tested. The back-pressure problem in global runs disappeared and the L1 rate was 96 kHz (consistent with the standard dead-time from other known sources).

Data network switches in the legacy HLT racks were replaced and have been configured and validated. New HLT server nodes have been extensively tested. They are ready from the DAQ side to deploy them for production. OMS continues running in production with availability close to 100% and is ready for LHC test beam runs. Support and new functionalities for sub-detectors continues to be developed. The OMS Data Warehouse is being improved. A new graduate student is working on improving the downtime analyzer and implementing new root plotter for better display of information at CERN.

Work on migrating Event builder RUBU nodes to run on RedHat 8 was originally planned in early 2022. However, because the team has been overwhelmed by commissioning activities since the start of the LHC run 3, this work is considered low priority at this moment and will be re-visited after 2022 run finishes during the year-end technical stop.

  ----------------------------------------------------------------
  Subsystem   Description                   Scheduled   Achieved
  ----------- ---------------------------- ----------- -----------
  DAQ         Storage and transfer system   Feb 2022    Feb 2022
              full commissioned

  DAQ         MiniDAQ 3 systems fully       Mar 2022    Mar 2022
              commissioned

  DAQ         Event builder RUBU nodes      Apr 2022    postponed
              running on RedHat 8                          to YETS

  DAQ         Assembly of DTH version P2v1  Apr 2022    Apr 2022

  DAQ         Installation of new HLT farm  Apr 2022    Apr 2022

  DAQ         New HLT farm full             May 2022    June 2022
              commissioned

  DAQ         Building 904: Retire Phase 1  July 2022
              DAQ hardware

  DAQ         First prototype of orbit      Dec 2022
              builder for HI-LHC

  DAQ         OMS system running on RedHat  Dec 2022
              8
  ----------------------------------------------------------------

  : DAQ 2022 Milestones


## Trigger

### Layer-1 Calorimeter Trigger

The Layer-1 Calorimeter trigger has successfully participated in the ongoing LHC tests at 900 GeV and is ready for data taking at 13.6 TeV. Various tests were performed on the HCAL and ECAL systems, where one ECAL and three HCAL channels had to be masked temporarily. This will need to re-checked before starting the stable data taking with nominal energy. The new configuration of the HCAL feature bits were checked and shown to be working as expected. The wide 5 BX readout for ECAL studies showed too high an occupancy and a low sensitivity to possible pre-firing effects. Therefore, it was decided to increase the threshold for tower energies to be stored in histograms, and a new pull request was prepared for this inclusion in the software, checked and included both in online and offline DQM. Trigger Primitive calibrations have been measured offline and the calibration schemed for the entire calorimeter trigger system has been agreed and implemented with the Layer 2 calorimeter trigger team.

### Muon Trigger

In the past quarter the commissioning of the muon trigger in the endcap (EMTF) has continued. One Trigger/DAQ control card (AMC13) had to be replaced after a failure during the Easter weekend. Feedback on the timing of the CSC, RPC, and GEM chambers has been provided to the muon teams. The CSC timing currently looks good at EMTF. The timing of the RPC data had to be delayed by 2 BX to be synchronized with CSC. Data from the GEM system still shows features observed before, namely a wider spread of timing over a few BX and some detector regions with high rates of noise. Tests at P5 and at B904 are following up on these features.

In the online software for EMTF, rate counters were added to monitor the CSC high multiplicity trigger. Additional link monitoring was also added.

The training and validation of the first Run 3 PT assignment look-up table using a Boosted Decision Tree is complete, and a new file has been uploaded to the EMTF system for P5 operations. The new training leads to improved resolution and better efficiency at high PT over the Run 2 table. Further training using the improvements in the Run 3 trigger primitives will continue.

On the firmware side, several fixes were implemented based on emulator-hardware disagreements observed in commissioning data. This included handling of trigger primitives from the new OTMBs versus the old TMBs, aligning the inputs to the neural network, and some additions to the DAQ output. Some adjustments to the firmware to pass along CSC high multiplicity trigger bits (used for a Long-Lived Particle trigger) independently of whether an LCT muon stub was found in a chamber also were implemented, as well as the suppression of these bits during electronics hard resets to avoid spurious rate. A few residual issues to address include the reworking the neural network firmware, used for displaced muon triggering, for shorter latency.

For all aspects of the EMTF other than the new LLP feature, the system is considered commissioned and ready for LHC high-energy collisions in July. Thus the commissioning milestone is considered met.

### Global Trigger and Central L1 Trigger Operations

The development and validation of the L1 trigger menu has been a priority for the start of LHC operations, and the group has been determining the rates of individual triggers and the total menu rate in order to optimize the physics program. New requests for L1 triggers, particularly for the B physics program, continue to arrive as do new calibrations for the calorimeters and calorimeter triggers. The group also continues to contribute to trigger shifts and L1 trigger expert on-call coverage.

### Field Operations Group of the Trigger Studies Group

This quarter the HLT Field Operations Group team successfully prepared and operated the trigger during 900 GeV test beam collisions. Accomplishments during this time period included testing several iterations of the evolving Run 3 HLT menu with 900 GeV collisions and testing GPU-based HLT reconstruction with real collisions. The FOG group provided on-call shifters for all data taking periods and exercised operational procedures for data taking, including data certification. In parallel, a number of improvements were made to the FOG software tools, especially RateMon, and software systems for collecting HLT and L1 trigger rate information were integrated together so the information can be viewed in one place. The FOG group is ready to operate the HLT during the physics data taking period with 13.6 TeV pp collisions coming up at the start of next quarter.

  ----------------------------------------------------------------------
  Subsystem   Description                           Scheduled   Achieved
  ----------- ----------------------------------- ----------- ----------
  TRIG        EMTF Commissioned                     Jun 2022   Jun 2022

  TRIG        EMTF Ready for Physics                Jul 2022 

  TRIG        EMTF Commissioned with GEM GE1/1      Dec 2022 
              inputs

  TRIG        BMTF Commissioned                     Jun 2022   Jun 2022

  TRIG        BMTF Ready for Physics                Jul 2022 

  TRIG        Calo Trig: Commissioned               Jun 2022   Jun 2022

  TRIG        Calo Trig: Ready for Physics          Jul 2022 

  TRIG        uGT emulator validated with           Jun 2022   Jun 2022
              firmware

  TRIG        uGT emulator ready for physics        Jul 2022 

  TRIG        uGT emulator implementation of new    Feb 2022   Feb 2022
              Run 3 algorithms

  TRIG        uGT emulator validated with LHC       Sep 2022
              data

  TRIG        Improved tools for jointly            Dec 2022
              configuring L1 and HLT prescales

  TRIG        Improved procedures to update HLT     Dec 2022
              rate monitoring reference fits

  TRIG        Improved P5 HLT software deployment   Dec 2022
              procedures
  ----------------------------------------------------------------------

  : Trigger 2022 Milestones

